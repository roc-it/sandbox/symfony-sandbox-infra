# Requirements
## Map hosts
```bash
# /etc/hosts
127.0.0.1 local.traefik.sandbox
127.0.0.1 local.symfony.sandbox
```

## SSL

Install [mkcert](https://github.com/FiloSottile/mkcert) and make sure you ran at least once
```bash
$ mkcert -install
```

After `make install` or `make certs`, two files will be created :
* `./common/env/local/certs/local-cert.pem`
* `./common/env/local/certs/local-key.pem`

# Install
## Env variables

After `make install` or `make setup`, several `*.env` files will be created.
One per docker service that needs configuring.
Modify them to your needs it won't be commit.

## Running the containers
### Local

If first install then :
```bash
$ make install
```

Otherwise simply :

```bash
$ make start
```

## Setting up the project locally
### Use the `functions.sh`
This file is to ease the use of containers without thinking about it when developing.

```bash
$ source ./functions.sh
```

### Install dependencies
```bash
$ composer install
```

### Setting up the database
```bash
$ dev do:mi:mi -n; dev h:f:l -n
```

### Build assets
#### React app
```bash
$ yarn install
$ yarn dev # or "watch" or "production"
```

## Blackfire
Create or log into your Blackfire account and access to [Blackfire docker integration documentation](https://blackfire.io/docs/integrations/docker/index) to copy paste your environments variables in `./app_main/blackfire.env` file.

:warning: For now Blackfire could not work with Xdebug enabled. Set `ENABLE_XDEBUG` to `false` if you want to profile with Blackfire.

Don't forget to install blackfire extension for Chrome or Firefox.

:warning: Blackfire extension isn't available for Safari

## Visualize docker dependencies graph

```bash
$ docker run --rm -v /var/run/docker.sock:/var/run/docker.sock leoverto/docker-network-graph | dot -Tsvg -o graph.svg
```

then open the file `graph.svg`
