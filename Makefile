SHELL          := /bin/bash
DOCKER         := docker
DOCKER_COMPOSE := docker compose
MKCERT         := mkcert

##
## Docker
## ------
##
certs:
	$(MKCERT) \
		-cert-file ./common/env/local/certs/local-cert.pem \
		-key-file ./common/env/local/certs/local-key.pem \
			"local.symfony.sandbox" \
			"local.traefik.sandbox"
.PHONY: certs

define APP_NGINX_ENV
SERVER_NAME="local.symfony.sandbox"
FPM_DSN=php:9000
endef
export APP_NGINX_ENV
./app_main/nginx.env:
	echo "$$APP_NGINX_ENV" > $@

define APP_NGINX_H2_ENV
ENABLE_HTTP2=true
SSL_CERT=/ssl/local-cert.pem
SSL_KEY=/ssl/local-key.pem
endef
export APP_NGINX_H2_ENV
./app_main/nginx_h2.env:
	echo "$$APP_NGINX_H2_ENV" > $@

define APP_PHP_ENV
PHP_XDEBUG_ENABLED=0
APP_SWITCH_ENV=true
endef
export APP_PHP_ENV
./app_main/php.env:
	echo "$$APP_PHP_ENV" > $@

define APP_POSTGRES_ENV
LC_ALL=C.UTF-8
POSTGRES_USER=symfony
POSTGRES_PASSWORD=symfony
POSTGRES_DB=symfony
endef
export APP_POSTGRES_ENV
./app_main/postgres.env:
	echo "$$APP_POSTGRES_ENV" > $@

define APP_VULCAIN_ENV
DEBUG=1
UPSTREAM=https://local.symfony.sandbox
KEY_FILE=tls/local-key.pem
CERT_FILE=tls/local-cert.pem
ADDR=:8080
endef
export APP_VULCAIN_ENV
./app_main/vulcain.env:
	echo "$$APP_VULCAIN_ENV" > $@

define APP_BLACKFIRE_ENV
BLACKFIRE_SERVER_ID=
BLACKFIRE_SERVER_TOKEN=
BLACKFIRE_CLIENT_ID=
BLACKFIRE_CLIENT_TOKEN=
endef
export APP_BLACKFIRE_ENV
./app_main/blackfire.env:
	echo "$$APP_BLACKFIRE_ENV" > $@

define DOT_ENV
# https://docs.docker.com/compose/reference/envvars/
COMPOSE_PROJECT_NAME=symfony-sandbox
COMPOSE_PATH_SEPARATOR=|
COMPOSE_FILE="./docker-compose.yml|./common/docker-compose.yml|./common/env/local/docker-compose.local.traefik.yml|./app_main/docker-compose.yml|./app_main/env/local/docker-compose.local.yml|./app_main/env/local/docker-compose.local.traefik.yml"

# Path to the mkcert CA file.
CA_ROOT_PATH="$(shell mkcert -CAROOT)"

# Fix permissions issues
APP_USER_ID=$(shell id -u)

SYMFONY_PROJECT_PATH="../symfony-sandbox"
endef
export DOT_ENV
./.env:
	echo "$$DOT_ENV" > $@

setup: ./.env ./app_main/blackfire.env ./app_main/nginx.env ./app_main/nginx_h2.env ./app_main/php.env ./app_main/postgres.env ./app_main/vulcain.env
.PHONY: setup

clone:
	source ./.env && (([ ! -d "$${SYMFONY_PROJECT_PATH}" ] && git clone git@gitlab.com:roc-it/sandbox/symfony-sandbox.git "$${SYMFONY_PROJECT_PATH}") || true)
.PHONY: clone

pull:
	$(DOCKER_COMPOSE) pull --include-deps --ignore-pull-failures
.PHONY: pull

build:
	$(DOCKER_COMPOSE) build
.PHONY: build

start:
	$(DOCKER_COMPOSE) up -d --remove-orphans --build
.PHONY: start

install: setup certs clone pull build start
.PHONY: install

.DEFAULT_GOAL := install
