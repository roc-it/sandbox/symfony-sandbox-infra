#!/usr/bin/env bash

## ---------------
## Setup
## ---------------
CURRENT_BASH=$(ps -p $$ | awk '{ print $4 }' | tail -n 1)
case "${CURRENT_BASH}" in
  -zsh|zsh)
    CURRENT_DIR=$(cd "$(dirname "${0}")" && pwd)
    ;;
  -bash|bash)
    CURRENT_DIR=$(cd "$(dirname ${BASH_SOURCE[0]})" && pwd)
    ;;
  *)
    echo 1>&2
    echo -e "\033[0;31m\`${CURRENT_BASH}\` does not seems to be supported\033[0m" 1>&2
    echo 1>&2
    return 1
    ;;
esac

source "${CURRENT_DIR}/../.env";

SYMFONY_PROJECT_ABSOLUTE_PATH=$(cd "${CURRENT_DIR}/../" && cd "${SYMFONY_PROJECT_PATH}" && pwd)

unalias __docker_exec 2>/dev/null >/dev/null || true
__docker_exec() {
    local absolute_subpath="${PWD}/"
    local subpath=""
    if [[ "${absolute_subpath}" == "${SYMFONY_PROJECT_ABSOLUTE_PATH}/"* ]]; then
        subpath=${absolute_subpath#"${SYMFONY_PROJECT_ABSOLUTE_PATH}/"}
    else
        subpath=""
    fi
    docker exec -it -w "/var/www/html/${subpath}" "$@"
}
export -f __docker_exec

## ---------------
## PHP & Tools
## ---------------
unalias php 2>/dev/null >/dev/null || true
php() {
  __docker_exec symfony_php bash -c "php $*"
}
export -f php

unalias composer 2>/dev/null >/dev/null || true
composer() {
  __docker_exec symfony_tools bash -c "COMPOSER_MEMORY_LIMIT=-1 composer $*"
}
export -f composer

unalias console 2>/dev/null >/dev/null || true
console() {
  php bin/console "$@"
}
export -f console

unalias phpunit 2>/dev/null >/dev/null || true
phpunit() {
  __docker_exec symfony_tools bash -c "./vendor/bin/phpunit $*"
}
export -f phpunit

unalias phpstan 2>/dev/null >/dev/null || true
phpstan() {
  __docker_exec symfony_tools bash -c "./vendor/bin/phpstan $*"
}
export -f phpstan

unalias rector 2>/dev/null >/dev/null || true
rector() {
  __docker_exec symfony_tools bash -c "./vendor/bin/rector $*"
}
export -f rector

## ---------------
## Symfony
## ---------------
unalias sf-check 2>/dev/null >/dev/null || true
sf-check() {
  php vendor/bin/requirements-checker
}
export -f sf-check

unalias dev 2>/dev/null >/dev/null || true
dev() {
  console --env=dev "$@"
}
export -f dev

unalias prod 2>/dev/null >/dev/null || true
prod() {
  console --env=prod "$@"
}
export -f prod

unalias test 2>/dev/null >/dev/null || true
test() {
  console --env=test "$@"
}
export -f test

## ---------------
## Postgres
## ---------------
unalias pgsql 2>/dev/null >/dev/null || true
pgsql() {
  source "${CURRENT_DIR}/postgres.env"
  docker exec -ti symfony_postgres sh -c "psql -U ${POSTGRES_USER} -d ${POSTGRES_DB} $*"
}
export -f pgsql

## ---------------
## Blackfire
## ---------------
unalias blackfire 2>/dev/null >/dev/null || true
blackfire() {
  docker exec -ti symfony_blackfire sh -c "blackfire $*"
}
export -f blackfire

## ---------------
## Node
## ---------------
unalias node 2>/dev/null >/dev/null || true
node() {
  __docker_exec symfony_node sh -c "node $*"
}
export -f node

unalias npm 2>/dev/null >/dev/null || true
npm() {
  __docker_exec symfony_node sh -c "npm $*"
}
export -f npm

unalias npx 2>/dev/null >/dev/null || true
npx() {
  __docker_exec symfony_node sh -c "npx $*"
}
export -f npx

unalias yarn 2>/dev/null >/dev/null || true
yarn() {
  __docker_exec symfony_node sh -c "yarn $*"
}
export -f yarn
